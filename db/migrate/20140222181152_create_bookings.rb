class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :renter_id
      t.integer :booker_id
      t.integer :spot_id
      t.datetime :check_in
      t.datetime :check_out

      t.timestamps
    end
  end
end
