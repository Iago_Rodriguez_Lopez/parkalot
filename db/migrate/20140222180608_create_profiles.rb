class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :name
      t.string :surname
      t.string :fullname
      t.string :telephone
      t.string :picture
      t.string :credit_account

      t.timestamps
    end
  end
end
