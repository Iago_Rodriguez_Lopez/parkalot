class AddAddressToSpot < ActiveRecord::Migration
  def change
    add_column :spots, :address, :string
    add_column :spots, :description, :string
    add_column :spots, :price, :integer 
  end
end
