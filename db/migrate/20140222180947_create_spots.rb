class CreateSpots < ActiveRecord::Migration
  def change
    create_table :spots do |t|
      t.integer :user_id
      t.string :license
      t.boolean :status
      t.string :image
      t.string :documantation
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
