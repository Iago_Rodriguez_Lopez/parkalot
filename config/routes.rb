Parkalot::Application.routes.draw do
  get 'livemap', to: 'home#livemap', as: 'livemap'
  get 'testmap', to: 'home#testmap', as: 'testmap'
  post 'book_path', to: 'spots#book', as: 'book'


  resources :bookings


  resources :spots


  resources :profiles


  	namespace :mobile do
 		get '', to: 'home#index', as: '/'
 
		authenticated :user do
		    root :to => 'home#index'
		end
		root :to => "home#index"
		devise_for :users
		resources :users  
	end

  authenticated :user do
    	root :to => 'home#index'
  end
  root :to => "home#index"
  devise_for :users
  resources :users
end