class HomeController < ApplicationController

	def livemap
		
		@points = Spot.all.map{|spot| [spot.longitude, spot.latitude] unless spot.longitude.nil? and spot.latitude.nil?}.compact
	end
end
