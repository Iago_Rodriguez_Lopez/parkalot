class SpotsController < ApplicationController
  # GET /spots
  # GET /spots.json
  def index
    @spots = Spot.all
    @points = Spot.all.map{|spot| [spot.longitude, spot.latitude] unless spot.longitude.nil? and spot.latitude.nil?}.compact

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @spots }
    end
  end

  # GET /spots/1
  # GET /spots/1.json
  def show
    @spot = Spot.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @spot }
    end
  end

  # GET /spots/new
  # GET /spots/new.json
  def new
    @spot = Spot.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @spot }
    end
  end

  # GET /spots/1/edit
  def edit
    @spot = Spot.find(params[:id])
  end

  # POST /spots
  # POST /spots.json
  def create
    @user = current_user
    @spot = @user.spots.new(params[:spot])

    respond_to do |format|
      if @spot.save
        format.html { redirect_to current_user, notice: 'Spot was successfully created.' }
        format.json { render json: @spot, status: :created, location: @spot }
      else
        format.html { render action: "new" }
        format.json { render json: @spot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /spots/1
  # PUT /spots/1.json
  def update
    @spot = Spot.find(params[:id])

    respond_to do |format|
      if @spot.update_attributes(params[:spot])
        format.html { redirect_to @spot, notice: 'Spot was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @spot.errors, status: :unprocessable_entity }
      end
    end
  end

  def book
    if user_signed_in?
      @spot = Spot.find(params[:spot][:id])
      #client = BitPay::Client.new 'YOUR_API_KEY'
      #invoice = client.post 'invoice', {:price => 10.00, :currency => 'USD'}
      redirect_to spot_path(@spot)
    else
      redirect_to new_user_registration_path, alert: "You have to register to book a spot!"
    end
  end

  def booked

  end

  # DELETE /spots/1
  # DELETE /spots/1.json
  def destroy
    @spot = Spot.find(params[:id])
    @spot.destroy

    respond_to do |format|
      format.html { redirect_to spots_url }
      format.json { head :no_content }
    end
  end
end
