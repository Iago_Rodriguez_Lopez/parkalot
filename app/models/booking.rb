class Booking < ActiveRecord::Base
  attr_accessible :booker_id, :check_in, :check_out, :renter_id, :spot_id
  belongs_to :renter, :class_name => 'User', :foreign_key => :renter_id
  belongs_to :booker, :class_name => 'User', :foreign_key => :booker_id
  belongs_to :spot

end
