class Spot < ActiveRecord::Base
  attr_accessible :documantation, :image, :latitude, :license, :longitude,:address, :price, :description,:status, :user_id
  belongs_to :user
	mount_uploader :image, SpotUploader
  	geocoded_by :address   # can also be an IP address
	after_validation :geocode
end
