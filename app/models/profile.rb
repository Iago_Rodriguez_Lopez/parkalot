class Profile < ActiveRecord::Base
  attr_accessible :credit_account, :fullname, :name, :picture, :surname, :telephone, :user_id
  belongs_to :user
end
