# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


$ ->
  $("#datetimepicker1").datetimepicker
    language: "en"
    pick12HourFormat: true

  $("#datetimepicker2").datetimepicker
    language: "en"
    pick12HourFormat: true

  return
